package publisher

import (
	"encoding/json"
	"errors"
	"regexp"

	"git.perkbox.io/go-shared-packages/go-hub-plugin/connection"
	"github.com/satori/uuid"
)

var eventRegex = regexp.MustCompile("^[A-Z]{3,}[-][a-zA-Z]{3,}$")

func Event(eventName string) *event {
	if connection.Client() == nil {
		return nil
	}

	if connection.PublicExchange() == "" {
		return nil
	}

	messageId := connection.PublicExchange() + "/" + eventName + "/" + uuid.NewV4().String()
	return &event{
		messageId:     messageId,
		correlationId: "",
		eventName:     eventName,
		priority:      1,
		payload:       make(map[string]interface{}),
	}
}

type event struct {
	messageId     string
	correlationId string
	eventName     string
	payload       map[string]interface{}
	priority      uint8
}

func (x *event) MessageId() string {
	return x.messageId
}

func (x *event) WithPriority(p uint8) *event {
	x.priority = p
	return x
}

func (x *event) WithPayload(p map[string]interface{}) *event {
	x.payload = p
	return x
}

func (x *event) WithCorrelationId(id string) *event {
	x.correlationId = id
	return x
}

func (x *event) Validate() error {
	if x.eventName == "" {
		return errors.New("eventName invalid")
	}

	if eventRegex.FindString(x.eventName) != x.eventName {
		return errors.New("eventName invalid")
	}

	// This is an RMQ specification
	if x.priority < 0 || x.priority > 9 {
		return errors.New("priority invalid")
	}

	return nil
}

func (x *event) MarshalJSON() ([]byte, error) {
	data := map[string]interface{}{
		"payload": x.payload,
	}

	return json.Marshal(data)
}
