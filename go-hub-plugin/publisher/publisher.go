package publisher

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"git.perkbox.io/go-shared-packages/go-hub-plugin/connection"
	"github.com/streadway/amqp"
)

// Send event to the exchange
func Publish(e *event) (returnErr error) {
	defer func() {
		if rec := recover(); rec != nil {
			returnErr = errors.New("publish panic:" + fmt.Sprint(rec))
		}
	}()

	if connection.Client() == nil {
		return errors.New("RMQ connection not initialised")
	}

	if e == nil {
		return errors.New("event == nil")
	}

	if err := e.Validate(); err != nil {
		return errors.New("event validation failed: " + err.Error())
	}

	body, err := json.Marshal(e)
	if err != nil {
		return err
	}

	return connection.MainChannel().Publish(
		connection.PublicExchange(),
		e.eventName, // routing key
		false,       // mandatory
		false,       // immediate
		amqp.Publishing{
			DeliveryMode:  amqp.Persistent,
			ContentType:   "application/json",
			Priority:      e.priority,
			MessageId:     e.messageId,
			Type:          e.eventName,
			CorrelationId: e.correlationId,
			Timestamp:     time.Now(),
			Body:          body,
		})
}
