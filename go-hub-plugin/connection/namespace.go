package connection

// On testing: ENV = {{ENV}} + "-" + {{BRANCH_ID}}

const (
	ExchangePublic    = "perkbox.exchange.{{ENV}}.public" // Public topic exchange, entry point for events
	ExchangeMain      = "perkbox.exchange.{{ENV}}.main"   // Internal direct exchange
	ExchangeRetry     = "perkbox.exchange.{{ENV}}.retry"  // Internal x-delayed-message type (underlying type: fanout) for failed events
	ExchangeGraveyard = "perkbox.exchange.{{ENV}}.graveyard"

	QueueGeneric   = "perkbox.queue.{{ENV}}.{{CONSUMER}}"
	QueueGraveyard = "perkbox.queue.{{ENV}}.graveyard"
)
