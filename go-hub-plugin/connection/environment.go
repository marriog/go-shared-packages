package connection

import (
	"errors"
	"fmt"
	"strings"
)

const (
	envTesting = "testing"
)

type RMQDetails struct {
	User        string
	Password    string
	Host        string
	VHost       string
	Environment string
	BranchID    string
	Verbose     bool
}

func (details RMQDetails) Validate() error {
	if details.User == "" || details.Password == "" || details.Host == "" || details.VHost == "" || details.Environment == "" || details.BranchID == "" {
		return errors.New("RMQ details missing")
	}

	return nil
}

func (details RMQDetails) Address() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s", details.User, details.Password, details.Host, details.VHost)
}

func (details RMQDetails) GetMainExchange() string {
	env := details.Environment

	if details.Environment == envTesting {
		// testing environment exchange name has to be unique based on branch id
		env = strings.ToLower(details.Environment + "-" + details.BranchID)
	}

	return strings.ToLower(strings.Replace(ExchangeMain, "{{ENV}}", env, 1))
}

func (details RMQDetails) GetPublicExchange() string {
	env := details.Environment

	if details.Environment == envTesting {
		// testing environment exchange name has to be unique based on branch id
		env = strings.ToLower(details.Environment + "-" + details.BranchID)
	}

	return strings.ToLower(strings.Replace(ExchangePublic, "{{ENV}}", env, 1))
}

func (details RMQDetails) GetRetryExchange() string {
	env := details.Environment

	if details.Environment == envTesting {
		// testing environment exchange name has to be unique based on branch id
		env = strings.ToLower(details.Environment + "-" + details.BranchID)
	}

	return strings.ToLower(strings.Replace(ExchangeRetry, "{{ENV}}", env, 1))
}

func (details RMQDetails) GetGraveyardExchange() string {
	env := details.Environment

	if details.Environment == envTesting {
		// testing environment exchange name has to be unique based on branch id
		env = strings.ToLower(details.Environment + "-" + details.BranchID)
	}

	return strings.ToLower(strings.Replace(ExchangeGraveyard, "{{ENV}}", env, 1))
}

func (details RMQDetails) GetQueue(namespace string) string {
	env := details.Environment

	if details.Environment == envTesting {
		// testing environment queue name has to be unique based on branch id
		env = strings.ToLower(details.Environment + "-" + details.BranchID)
	}

	final := strings.Replace(QueueGeneric, "{{ENV}}", env, 1)
	return strings.ToLower(strings.Replace(final, "{{CONSUMER}}", namespace, 1))
}

func (details RMQDetails) GetGraveyardQueue() string {
	env := details.Environment

	if details.Environment == envTesting {
		// testing environment queue name has to be unique based on branch id
		env = strings.ToLower(details.Environment + "-" + details.BranchID)
	}

	return strings.ToLower(strings.Replace(QueueGraveyard, "{{ENV}}", env, 1))
}
