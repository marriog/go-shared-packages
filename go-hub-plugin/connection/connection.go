package connection

import (
	"errors"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/satori/uuid"
	"github.com/streadway/amqp"
)

var (
	cli        *client
	rmqDetails *RMQDetails
)

type client struct {
	serviceName       string
	mainExchange      string // internal
	publicExchange    string // public
	retryExchange     string // retry (internal)
	graveyardExchange string // receives all events that fail their max retries
	graveyardQueue    string
	connection        *amqp.Connection
	mainChannel       *amqp.Channel
	crash             chan *amqp.Error
}

func Shutdown(stopWg *sync.WaitGroup) {
	defer stopWg.Done()
	cli.mainChannel.Close()
}

func Bootstrap(details *RMQDetails) error {
	cli = new(client)
	cli.crash = make(chan *amqp.Error)

	if details == nil {
		details = &RMQDetails{
			User:        os.Getenv("RMQ_USER"),
			Password:    os.Getenv("RMQ_PASSWORD"),
			Host:        os.Getenv("RMQ_HOST"),
			VHost:       os.Getenv("RMQ_VHOST"),
			Environment: os.Getenv("ENVIRONMENT"),
			BranchID:    os.Getenv("BRANCH_ID"),
		}
	}

	if err := details.Validate(); err != nil {
		return err
	}

	cli.publicExchange = details.GetPublicExchange()
	cli.mainExchange = details.GetMainExchange()
	cli.retryExchange = details.GetRetryExchange()
	cli.graveyardExchange = details.GetGraveyardExchange()
	cli.graveyardQueue = details.GetGraveyardQueue()

	/********************/
	/* CONNECTION       */
	/********************/

	// Open connection
	var err error
	cli.connection, err = amqp.Dial(details.Address())
	if err != nil {
		return errors.New("failed to open connection:" + err.Error())
	}

	// Connect to channel
	cli.mainChannel, err = cli.connection.Channel()
	if err != nil {
		return errors.New("failed to connect to channel:" + err.Error())
	}

	if details.Verbose == true {
		fmt.Fprint(os.Stdout, "[RMQ] Connected\n")
	}

	/********************/
	/* PUBLIC EXCHANGE  */
	/********************/

	err = cli.mainChannel.ExchangeDeclare(cli.publicExchange, "topic", true, false, false, false, nil)
	if err != nil {
		return errors.New("failed to set up PUBLIC exchange:" + err.Error())
	}

	if details.Verbose == true {
		fmt.Fprint(os.Stdout, "[RMQ] Using PUBLIC exchange "+cli.publicExchange+"\n")
	}

	/********************/
	/* MAIN EXCHANGE    */
	/********************/

	err = cli.mainChannel.ExchangeDeclare(cli.mainExchange, "direct", true, false, true, false, nil) // internal = true
	if err != nil {
		return errors.New("failed to set up MAIN exchange:" + err.Error())
	}

	if details.Verbose == true {
		fmt.Fprint(os.Stdout, "[RMQ] Using MAIN exchange "+cli.mainExchange+"\n")
	}

	/********************/
	/* RETRY EXCHANGE    */
	/********************/

	// Declare exchange
	// TYPE: x-delayed-message fanout
	err = cli.mainChannel.ExchangeDeclare(cli.retryExchange, "x-delayed-message", true, false, false, false, amqp.Table{"x-delayed-type": "fanout"})
	if err != nil {
		return errors.New("failed to set up RETRY exchange:" + err.Error())
	}

	if details.Verbose == true {
		fmt.Fprint(os.Stdout, "[RMQ] Using RETRY exchange "+cli.retryExchange+"\n")
	}

	/*************************/
	/* BIND PUBLIC --> MAIN  */
	/*************************/

	// Public is a topic exchange
	err = cli.mainChannel.ExchangeBind(cli.mainExchange, "#", cli.publicExchange, false, nil)
	if err != nil {
		return errors.New("failed to set up PUBLIC --> MAIN exchange binding:" + err.Error())
	}

	/************************/
	/* BIND RETRY --> MAIN  */
	/************************/

	// Retry is a fanout exchange
	err = cli.mainChannel.ExchangeBind(cli.mainExchange, "", cli.retryExchange, false, nil)
	if err != nil {
		return errors.New("failed to set up RETRY --> MAIN exchange binding:" + err.Error())
	}

	/************************/
	/* GRAVEYARD            */
	/************************/

	err = cli.mainChannel.ExchangeDeclare(cli.graveyardExchange, "fanout", true, false, false, false, nil)
	if err != nil {
		return errors.New("failed to set up GRAVEYARD exchange:" + err.Error())
	}

	if details.Verbose == true {
		fmt.Fprint(os.Stdout, "[RMQ] Using GRAVEYARD exchange "+cli.graveyardExchange+"\n")
	}

	_, err = cli.mainChannel.QueueDeclare(
		cli.graveyardQueue, // name
		true,               // durable
		false,              // delete when unused
		false,              // exclusive
		false,              // no-wait
		nil,                // arguments
	)
	if err != nil {
		return errors.New("failed to declare GRAVEYARD queue:" + err.Error())
	}

	err = cli.mainChannel.QueueBind(
		cli.graveyardQueue,    // queue name
		"",                    // routing key (event name)
		cli.graveyardExchange, // exchange
		false,
		nil)

	if err != nil {
		return errors.New("failed to bind GRAVEYARD exchange --> queue:" + err.Error())
	}

	/********************/
	/* ETC              */
	/********************/

	cli.crash = cli.connection.NotifyClose(cli.crash)
	rmqDetails = details

	return nil
}

func Client() *client {
	return cli
}

func Details() *RMQDetails {
	return rmqDetails
}

func CrashWatcher() chan *amqp.Error {
	return cli.crash
}

func PublicExchange() string {
	return cli.publicExchange
}

func MainExchange() string {
	return cli.mainExchange
}

func RetryExchange() string {
	return cli.retryExchange
}

func GraveyardExchange() string {
	return cli.graveyardExchange
}

func Connection() *amqp.Connection {
	return cli.connection
}

func MainChannel() *amqp.Channel {
	return cli.mainChannel
}

func Ping() error {
	if cli == nil {
		return errors.New("RMQ connection not initialised")
	}

	return cli.mainChannel.Publish(
		cli.publicExchange,
		"SYSTEM-Ping", // routing key
		false,         // mandatory
		false,         // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Priority:     8,
			MessageId:    "SYSTEM-Ping/" + uuid.NewV4().String(),
			Type:         "SYSTEM-Ping",
			Timestamp:    time.Now(),
			Body:         []byte(`{}`),
		})
}
