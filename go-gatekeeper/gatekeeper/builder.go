package gatekeeper

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"time"

	"git.perkbox.io/go-shared-packages/go-error/perkboxError"
	"github.com/dgrijalva/jwt-go"
)

type TokenData struct {
	UserID          string
	Email           string
	Tenant          string
	PermissionsJSON []byte
	ExpiresIn       int                    // optional (defaults to 1800)
	IssuedAt        int                    // optional (if you need to set IAT)
	SecretKey       []byte                 // optional (defaults to tokenSecret var)
	CustomClaims    map[string]interface{} // optional
}

// Build an access token
func BuildToken(data TokenData) (string, perkboxError.Error) {

	// Default token secret
	if data.SecretKey == nil {
		data.SecretKey = tokenSecret
	}

	// Default expiry time
	if data.ExpiresIn <= 0 {
		data.ExpiresIn = 1800
	}

	// Default IAT
	if data.IssuedAt <= 0 {
		data.IssuedAt = int(time.Now().Unix())
	}

	claims := jwt.MapClaims{
		"iss": "perkbox",
		"iat": data.IssuedAt,
		"sub": data.UserID,
		"exp": data.IssuedAt + data.ExpiresIn,
		"usr": data.Email,
		"ten": data.Tenant,
	}

	if data.CustomClaims != nil {
		for k, v := range data.CustomClaims {
			claims[k] = v
		}
	}

	// Replace special strings (if any)
	permz := replaceSpecialStrings(data.PermissionsJSON, claims)

	// Permissions
	zipped := zipString(permz)
	encoded := base64.StdEncoding.EncodeToString(zipped)
	claims["aut"] = encoded

	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	result, err := newToken.SignedString(data.SecretKey)
	if err != nil {
		return "", perkboxError.New(perkboxError.TokenBuilderError).WithInternal("signing token failed: " + err.Error()).WithStatus(500)
	}

	return result, nil
}

func zipString(data []byte) []byte {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)

	gz.Write(data)
	gz.Flush()
	gz.Close()

	return b.Bytes()
}
