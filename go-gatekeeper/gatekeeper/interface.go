package gatekeeper

import "git.perkbox.io/go-shared-packages/go-error/perkboxError"

type IToken interface {
	Seek(service, resource, action string) (*Action, perkboxError.Error)
	Get(claim string) (interface{}, error)
	UserId() string
	Email() string
	Tenant() string
}
