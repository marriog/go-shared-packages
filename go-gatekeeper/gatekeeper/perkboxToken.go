package gatekeeper

import (
	"errors"
	"fmt"

	"git.perkbox.io/go-shared-packages/go-error/perkboxError"

	"github.com/dgrijalva/jwt-go"
)

type PerkboxToken struct {
	claims      jwt.MapClaims
	permissions map[string]interface{}
}

func NewToken(c jwt.MapClaims, p map[string]interface{}) *PerkboxToken {
	return &PerkboxToken{claims: c, permissions: p}
}

// Find a specific permission
// Return Action object populated with list of restrictions (if any)
func (pb PerkboxToken) Seek(service, resource, action string) (*Action, perkboxError.Error) {
	missingErr := perkboxError.New(perkboxError.ForbiddenNoPermission).WithInternal(fmt.Sprintf("%s - %s - %s missing", service, resource, action)).WithStatus(403)

	serviceItemRaw, exists := pb.permissions[service]
	if exists == false {
		return nil, missingErr
	}

	serviceItem, ok := serviceItemRaw.(map[string]interface{})
	if ok == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("invalid json structure in service: " + service).WithStatus(401)
	}

	resourceItemRaw, exists := serviceItem[resource]
	if exists == false {
		return nil, missingErr
	}

	resourceItem, ok := resourceItemRaw.(map[string]interface{})
	if ok == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("invalid json structure in resource: " + resource).WithStatus(401)
	}

	actionItemRaw, exists := resourceItem[action]
	if exists == false {
		return nil, missingErr
	}

	actionItem, ok := actionItemRaw.(map[string]interface{})
	if ok == false {
		if _, ok := actionItemRaw.([]interface{}); ok == false {
			return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("invalid json structure in action: " + action).WithStatus(401)
		}

		actionItem = map[string]interface{}{}
	}

	a, err := buildAction(actionItem)
	if err != nil {
		return nil, err
	}

	return a, nil
}

// Retrieve a claim value from the token
func (pb PerkboxToken) Get(claim string) (interface{}, error) {
	item, exists := pb.claims[claim]
	if exists == false {
		return "", errors.New("claim not found:" + claim)
	}

	return item, nil
}

// Retrieve userId from the token
func (pb PerkboxToken) UserId() string {
	item, exists := pb.claims["sub"]
	if exists == false {
		return ""
	}

	str, ok := item.(string)
	if ok == false {
		return ""
	}

	return str
}

// Retrieve email from the token
func (pb PerkboxToken) Email() string {
	item, exists := pb.claims["usr"]
	if exists == false {
		return ""
	}

	str, ok := item.(string)
	if ok == false {
		return ""
	}

	return str
}

// Retrieve tenant from the token
func (pb PerkboxToken) Tenant() string {
	item, exists := pb.claims["ten"]
	if exists == false {
		return ""
	}

	str, ok := item.(string)
	if ok == false {
		return ""
	}

	return str
}

func (pb PerkboxToken) RawPermissions() map[string]interface{} {
	return pb.permissions
}
