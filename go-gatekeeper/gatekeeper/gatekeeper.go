package gatekeeper

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"git.perkbox.io/go-shared-packages/go-error/perkboxError"
	"github.com/dgrijalva/jwt-go"
)

func Retrieve(r *http.Request) *PerkboxToken {
	raw := r.Context().Value(contextKey)
	if raw == nil {
		return nil
	}

	pbToken, ok := raw.(*PerkboxToken)
	if !ok {
		return nil
	}

	return pbToken
}

func Attach(r *http.Request) (*http.Request, perkboxError.Error) {
	// Get token from header
	auth := r.Header.Get("Authorization")
	if auth == "" {
		return r, nil
	}

	// Left trim "Bearer " from the token
	auth = strings.TrimPrefix(auth, "Bearer ")
	auth = strings.TrimPrefix(auth, "bearer ")

	// Validate access token
	perkboxToken, err := Auth(auth)
	if err != nil {
		return r, err
	}

	c := context.WithValue(r.Context(), contextKey, perkboxToken)
	modified := r.WithContext(c)

	return modified, nil
}

func Auth(authHeader string) (*PerkboxToken, perkboxError.Error) {
	accessToken, err := unlockJwt(authHeader, false)
	if err != nil {
		return nil, err
	}

	claims, err := validateClaims(accessToken)
	if err != nil {
		return nil, err
	}

	permissions, err := unwrapPermissions(claims)
	if err != nil {
		return nil, err
	}

	return &PerkboxToken{claims: claims, permissions: permissions}, nil
}

func AuthWithoutExpiry(authHeader string) (*PerkboxToken, perkboxError.Error) {
	accessToken, err := unlockJwt(authHeader, true)
	if err != nil {
		return nil, err
	}

	claims, err := validateClaims(accessToken)
	if err != nil {
		return nil, err
	}

	permissions, err := unwrapPermissions(claims)
	if err != nil {
		return nil, err
	}

	return &PerkboxToken{claims: claims, permissions: permissions}, nil
}

// Unlock the JWT token with the secret key
// Validate token
func unlockJwt(auth string, ignoreExpiry bool) (*jwt.Token, perkboxError.Error) {
	parser := new(jwt.Parser)
	if ignoreExpiry {
		parser.SkipClaimsValidation = true
	}

	unlocked, err := parser.Parse(auth, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal(fmt.Sprintf("unexpected signing method: %v", token.Header["alg"])).WithStatus(401)
		}

		return tokenSecret, nil
	})

	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok == true {

			switch ve.Errors {

			case jwt.ValidationErrorMalformed:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("not a valid jwt token").WithStatus(401)
			case jwt.ValidationErrorExpired:
				return nil, perkboxError.New(perkboxError.ForbiddenTokenExpired).WithStatus(401)
			case jwt.ValidationErrorNotValidYet:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("token not valid yet").WithStatus(401)
			case jwt.ValidationErrorUnverifiable:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("token unverifiable").WithStatus(401)
			case jwt.ValidationErrorSignatureInvalid:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("signature invalid").WithStatus(401)
			case jwt.ValidationErrorAudience:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("AUD validation failed").WithStatus(401)
			case jwt.ValidationErrorIssuedAt:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("IAT validation failed").WithStatus(401)
			case jwt.ValidationErrorIssuer:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("ISS validation failed").WithStatus(401)
			case jwt.ValidationErrorId:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("JTI validation failed").WithStatus(401)
			case jwt.ValidationErrorClaimsInvalid:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("claims validation failed").WithStatus(401)
			default:
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("unknown error").WithStatus(401)
			}

		} else {
			return nil, perkboxError.New(perkboxError.UnknownError).WithInternal("jwt validation:" + err.Error()).WithStatus(401)
		}
	}

	return unlocked, nil
}

// Check for all required claims
func validateClaims(accessToken *jwt.Token) (jwt.MapClaims, perkboxError.Error) {
	claims, ok := accessToken.Claims.(jwt.MapClaims)
	if ok == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("jwt library error").WithStatus(401)
	}

	if iss, exists := claims["iss"]; exists == false || iss != "perkbox" {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("not a perkbox token (invalid issuer)").WithStatus(401)
	}

	if _, exists := claims["aut"]; exists == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("claim missing: aut").WithStatus(401)
	}

	if _, exists := claims["usr"]; exists == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("claim missing: usr").WithStatus(401)
	}

	if _, exists := claims["sub"]; exists == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("claim missing: sub").WithStatus(401)
	}

	if _, exists := claims["ten"]; exists == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("claim missing: ten").WithStatus(401)
	}

	return claims, nil
}

// Decode & decompress the permissions string
// Returns a map of permissions
func unwrapPermissions(claims jwt.MapClaims) (map[string]interface{}, perkboxError.Error) {
	base64permissions, ok := claims["aut"].(string)
	if ok == false {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("AUT not a string").WithStatus(401)
	}

	zippedPermissions, err := base64.StdEncoding.DecodeString(base64permissions)
	if err != nil {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("base64 decode failed: " + err.Error()).WithStatus(401)
	}

	gr, err := gzip.NewReader(bytes.NewBuffer(zippedPermissions))
	defer gr.Close()
	data, err := ioutil.ReadAll(gr)
	if err != nil {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("unzip failed: " + err.Error()).WithStatus(401)
	}

	// Replace special strings (if any)
	data = replaceSpecialStrings(data, claims)

	decoded := make(map[string]interface{})
	err = json.Unmarshal(data, &decoded)
	if err != nil {
		return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("permissions invalid JSON").WithStatus(401)
	}

	return decoded, nil
}

func replaceSpecialStrings(in []byte, claims map[string]interface{}) []byte {
	tmpData := string(in)
	tmpData = strings.Replace(tmpData, "$[token:tenant]", claims["ten"].(string), -1)
	tmpData = strings.Replace(tmpData, "$[token:sub]", claims["sub"].(string), -1)
	tmpData = strings.Replace(tmpData, "$[token:email]", claims["usr"].(string), -1)

	subdomain := "!undefined"
	if tsd, ok := claims["tsd"]; ok {
		subdomain = tsd.(string)
	}
	tmpData = strings.Replace(tmpData, "$[token:subdomain]", subdomain, -1)

	return []byte(tmpData)
}
