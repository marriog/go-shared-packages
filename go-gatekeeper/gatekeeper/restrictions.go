package gatekeeper

import "fmt"
import (
	"strings"

	"git.perkbox.io/go-shared-packages/go-error/perkboxError"
)

const (
	Whitelist = '*'
	Blacklist = '!'
)

type Action struct {
	restrictions []Restriction
}

type Restriction struct {
	rType    byte
	logicAnd bool
	key      string
	values   []string
}

func buildAction(data map[string]interface{}) (*Action, perkboxError.Error) {
	r := make([]Restriction, 0)

	for rKey, rVal := range data {

		// KEY
		if rKey[0] != Whitelist && rKey[0] != Blacklist {
			return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("invalid restriction prefix in action").WithStatus(401)
		}

		current := new(Restriction)
		current.rType = rKey[0]
		current.key = rKey[1:]
		current.logicAnd = false

		// VALUES
		var convertedVals []interface{}
		var strVals []string

		convertedVals, ok := rVal.([]interface{})
		if !ok {
			_strVals, ok2 := rVal.([]string)
			if !ok2 {
				return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("restriction values not a slice").WithStatus(401)
			}
			strVals = _strVals
		} else {
			strVals = make([]string, len(convertedVals))
			for i := 0; i < len(convertedVals); i++ {
				v, ok := convertedVals[i].(string)
				if !ok {
					return nil, perkboxError.New(perkboxError.ForbiddenInvalidToken).WithInternal("restriction values string conversion error").WithStatus(401)
				}
				strVals[i] = v
			}
		}

		current.values = strVals
		r = append(r, *current)
	}

	return &Action{restrictions: r}, nil
}

func (a Action) Restrictions() []Restriction {
	return a.restrictions
}

func (r *Restriction) LogicOr() *Restriction {
	r.logicAnd = false
	return r
}

func (r *Restriction) LogicAnd() *Restriction {
	r.logicAnd = true
	return r
}

func (r Restriction) IsWhitelist() bool {
	return r.rType == Whitelist
}

func (r Restriction) IsBlacklist() bool {
	return r.rType == Blacklist
}

func (r Restriction) Key() string {
	return r.key
}

func (r Restriction) Values() []string {
	return r.values
}

func (r Restriction) ValuesAsString() string {
	return fmt.Sprint(r.values)
}

func (r Restriction) IsSatisfiedBy(items ...string) bool {
	// Example
	// Read deals restriction: "*tags": ["A", "B", "C", "D"]

	// IF logicAnd == FALSE (default)
	// At least 1 item has to satisfy the restriction
	// Deal has tags [A, X, Y, Z] => TRUE
	// Deal has tags [X, Y, Z] => FALSE

	// IF logicAnd == TRUE
	// All items have to satisfy the restriction
	// Deal has tags [A, B, C] => TRUE
	// Deal has tags [A, B, C, D, E, F] => FALSE

	toSatisfy := len(items)
	satisfied := 0

	if len(items) == 0 || len(r.values) == 0 {
		return false
	}

	for _, needle := range items {
		for _, val := range r.values {
			if val == needle {
				satisfied++
			}
		}
	}

	if r.logicAnd == true {
		return satisfied >= toSatisfy
	} else {
		return satisfied > 0
	}
}

func (r Restriction) Evaluate(items ...string) perkboxError.Error {
	satisfied := r.IsSatisfiedBy(items...)
	itemsStr := strings.Join(items, ",")

	if r.IsWhitelist() && !satisfied {
		return perkboxError.New(perkboxError.ForbiddenRestrictionCheckFailed).WithInternal("have:" + itemsStr + " | whitelist:" + r.ValuesAsString()).WithStatus(403)
	}

	if r.IsBlacklist() && satisfied {
		return perkboxError.New(perkboxError.ForbiddenRestrictionCheckFailed).WithInternal("have:" + itemsStr + " | blacklist:" + r.ValuesAsString()).WithStatus(403)
	}

	return nil
}
