# healthzprom

Package healthzprom provides HTTP handler for service health checking using
Prometheus response format.

## Installation

```go
import "git.perkbox.io/backend-services/shared/go-metrics/healthzprom"
```

## Usage

To add Prometheus health check handler with JSON-format fallback

```go
import (
    "git.perkbox.io/go-shared-packages/go-metrics/healthzprom"
    "git.perkbox.io/go-shared-packages/go-metrics/healthzhttp"
)

// ...

jsonHandler := healthzhttp.NewHandler(checker, logger)
promHandler := healthzprom.NewHandler(checker)
promWrapper := healthzprom.Middleware(promHandler)
http.Handle("/healthz", promWrapper(jsonHandler)))
```
