// Package healthzprom provides HTTP handler for service health checking using
// Prometheus response format.
package healthzprom

import (
	"html/template"
	"net/http"
	"regexp"

	"git.perkbox.io/go-shared-packages/go-metrics/healthz"
	"git.perkbox.io/go-shared-packages/go-metrics/healthzhttp"
)

// Template is the response template.
const Template = `# HELP perkbox_service_healthcheck_error Status of service health checks
# TYPE perkbox_service_healthcheck_error gauge
{{- range .}}
perkbox_service_healthcheck_error{service_name="{{.Service}}", error_code="{{.Status.ErrorCode}}"} {{if .Status.StatusOK}}0{{else}}1{{end}}
{{- end}}
`

var tmpl = template.Must(template.New("prometheus").Parse(Template))
var regex = regexp.MustCompile(`^Prometheus\/.+`)

// Handler responds to HTTP health check requests.
type Handler struct {
	chkr healthz.Checker
}

var _ http.Handler = &Handler{}

// NewHandler returns new HTTP health check handler.
func NewHandler(chkr healthz.Checker) *Handler {
	return &Handler{chkr: chkr}
}

// ServeHTTP implements net/http.ServeHTTP.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	tmpl.Execute(w, h.chkr.Check(r.Context(), r.FormValue("service")))
}

// Middleware intercepts the requests with header "User-Agent: Prometheus/*".
func Middleware(p *Handler) healthzhttp.Middleware {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if regex.MatchString(r.Header.Get("User-Agent")) {
				p.ServeHTTP(w, r)
				return
			}
			h.ServeHTTP(w, r)
		})
	}
}
