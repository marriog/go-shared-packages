// Package healthzhttp provides HTTP handler for service health checking.
package healthzhttp

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"git.perkbox.io/go-shared-packages/go-metrics/healthz"
)

// Handler responds to HTTP health check requests.
type Handler struct {
	chkr   healthz.Checker
	logger *log.Logger
}

var _ http.Handler = &Handler{}

// NewHandler returns new HTTP health check handler.
func NewHandler(chkr healthz.Checker, logger *log.Logger) *Handler {
	return &Handler{
		chkr:   chkr,
		logger: logger,
	}
}

// Result represet the health check status of a service.
type Result struct {
	Service string `json:"service_name"`
	healthz.Status
}

// ServeHTTP implements net/http.ServeHTTP.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	res := h.chkr.Check(r.Context(), r.FormValue("service"))

	w.Header().Set("Content-Type", "application/json")

	if len(res) < 1 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, `"Service not found"`)
		return
	}

	resp := []Result{}
	for _, r := range res {
		if !r.Status.StatusOK {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
		resp = append(resp, Result{
			Service: r.Service,
			Status:  r.Status,
		})
	}
	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		h.logger.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%q", http.StatusText(http.StatusInternalServerError))
	}
}

// Middleware is HTTP handler middleware.
type Middleware func(http.Handler) http.Handler
