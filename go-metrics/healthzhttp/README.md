# healthzhttp

Package healthzhttp provides HTTP handler for service health checking.

## Installation

```go
import "git.perkbox.io/go-shared-packages/go-metrics/healthzhttp"
```

## Usage

To register a generic JSON-formatted health check reporting handler

```go
import (
    "net/http"

    "git.perkbox.io/go-shared-packages/go-metrics/healthzhttp"
)

// ...

handler := healthzhttp.NewHandler(checker, logger)
http.Handle("/healthz", handler)
```
