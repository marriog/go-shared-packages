// Package healthz provides tools for service health checking.
package healthz

import (
	"context"
	"fmt"
	"sync"
)

// ErrCodeCancelled is the error code used when a health check was cancelled.
const ErrCodeCancelled = "CANCELLED"

// Status represents a health check status.
type Status struct {
	StatusOK         bool   `json:"status"`
	ErrorCode        string `json:"error_code"`
	BusinessImpact   string `json:"business_impact,omitempty"`
	TechnicalSummary string `json:"technical_summary,omitempty"`
	PanicGuide       string `json:"panic_guide,omitempty"`
}

// Check represents a service health check.
type Check func(context.Context) Status

// Service represents a service, which can be health-checked.
type Service struct {
	Name  string
	Check Check
}

// Result represents the result of a health check for a service.
type Result struct {
	Service string
	Status  Status
}

// ServiceMatcher is type which returns the services matching name.
type ServiceMatcher func(name string, services []Service) []Service

// ExactMatcher returns the services with name.
func ExactMatcher(name string, services []Service) []Service {
	matches := []Service{}
	for _, s := range services {
		if s.Name == name {
			matches = append(matches, s)
		}
	}
	return matches
}

var _ ServiceMatcher = ExactMatcher

// AllMatcher returns all services, regardless of their name.
func AllMatcher(name string, services []Service) []Service {
	return services
}

var _ ServiceMatcher = AllMatcher

// Strategy decides wether to stop further health checks or to continue.
type Strategy interface {
	Continue(Result) bool
}

// StopOnFailure is the health check strategy in which the service is considered
// unhealthy when there is a failing check.
type StopOnFailure struct{}

var _ Strategy = StopOnFailure{}

// Continue returns true if the status of the health check is not ok.
func (s StopOnFailure) Continue(r Result) bool {
	return r.Status.StatusOK
}

// ContinueOnFailure is the health check strategy to run all health checks
// regardless of their status.
type ContinueOnFailure struct{}

var _ Strategy = ContinueOnFailure{}

// Continue returns true.
func (s ContinueOnFailure) Continue(Result) bool {
	return true
}

// Checker runs the health checks for service and returns the result.
type Checker interface {
	Check(ctx context.Context, service string) []Result
}

type checker struct {
	matcher  ServiceMatcher
	strategy Strategy
	services []Service
}

var _ Checker = &checker{}

// NewChecker returns new health check server.
func NewChecker(matcher ServiceMatcher, strategy Strategy, services ...Service) Checker {
	return &checker{
		matcher:  matcher,
		strategy: strategy,
		services: services,
	}
}

// Check runs the health checks for service and returns the result.
func (c *checker) Check(ctx context.Context, service string) []Result {
	results := []Result{}
	svc := c.matcher(service, c.services)
	ctx, cancel := context.WithCancel(ctx)
	wg := sync.WaitGroup{}
	ch := make(chan Result)

	// Send cancellation signal to interrupt running health checks.
	defer cancel()

	// Run each service check in a goroutine.
	for _, svc := range svc {
		wg.Add(1)
		svc := svc
		go func() {
			select {
			case ch <- Result{Service: svc.Name, Status: svc.Check(ctx)}:
			case <-ctx.Done():
				ch <- Result{
					Service: svc.Name,
					Status: Status{
						ErrorCode:        ErrCodeCancelled,
						TechnicalSummary: fmt.Sprintf("health check cancelled: %s", ctx.Err()),
					},
				}
			}
			wg.Done()
		}()
	}

	// Close ch channel when all health check are done in order to prevent
	// blocking of the result checks.
	go func() {
		wg.Wait()
		close(ch)
	}()

	// Check the result of each health check.
	for r := range ch {
		results = append(results, r)
		if !c.strategy.Continue(r) {
			return results
		}
	}

	return results
}
