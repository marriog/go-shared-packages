# healthz

Package healthz provides tools for service health checking.

## Installation

```go
import "git.perkbox.io/go-shared-packages/go-metrics/healthz"
```

## Usage

Define your service health checks

```go
name := os.Getenv("PERKBOX_SERVICE_NAME")
mysqlHealth := healthz.Service{
  Name: name,
  Check: (ctx context.Context) healthz.Status {
    err := db.PingContext(ctx)
    return healthz.Status{
        StatusOK:         err == nil,
        ErrorCode:        fmt.Sprintf("%s-MYSQL-PING", name),
        BusinessImpact:   "Service unavailable",
        TechnicalSummary: "MySQL database cannot be reached",
        PanicGuide:       "Run for your life",
    }
  },
}
```

Register your health checks

```go
checker := healthz.NewChecker(
    healthz.AllMatcher, // ... or healthz.ExactMatcher?
    healthz.ContinueOnFailure{}, // ... healthz.StopOnFailure?
    mysqlHealth,
    // ... more health checks
)
```
