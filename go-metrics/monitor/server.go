package monitor

import (
	"context"
	"net"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"sync"
)

const (
	metricsServerAddr = ":9145"
)

func StartMetricsServer(stopWg *sync.WaitGroup, shutdownCh chan struct{}, errCh chan error) {
	defer stopWg.Done()
	
	// Prometheus metrics endpoint
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())

	httpServer := http.Server{
		Addr:    metricsServerAddr,
		Handler: mux,
	}

	startErrCh := make(chan error)
	startFunc := func() {
		listener, err := net.Listen("tcp", metricsServerAddr)
		if err != nil {
			startErrCh <- err
			return
		}
		httpServer.Serve(listener)
	}

	stopFunc := func() {
		// Gracefully shutdown http server
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		httpServer.Shutdown(ctx)
	}

	go startFunc()

	select {
	case err := <-startErrCh:
		errCh <- err
	case <-shutdownCh:
		stopFunc()
	}
}
