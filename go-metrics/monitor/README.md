# monitor

## Metrics endpoint

The metrics server has to be started on port 9145. Depending on how you are
bootstrapping your services you can use one of the following implementations.

### ALT #1: Add prometheus endpoint with your own implementation

Start a new HTTP server on port 9145 and add the following handler (from
Prometheus go library)

```go
mux.Handle("/metrics", promhttp.Handler())
```

### ALT #2: Use of provided server with controlled graceful shutdown

> **NOTE**: Every server added to the waitGroup **MUST** call stopWg.Done() when
> its shutdown is complete.

If you have multiple server running, the following snippet shows a possible
implementation of the bootstrap that will use a single shutdown channel
(triggered by an error or an OS signal) for all the servers and wait for each of
them to stop gracefully.

```go
var stopWg sync.WaitGroup

errCh := make(chan error, 1)
shutdownCh := make(chan struct{}, 1)

// Start servers
stopWg.Add(1)
go monitor.StartMetricsServer(&stopWg, shutdownCh, errCh)

// Sample code for other service servers initialization
// (each server must add to the waitgroup counter)
stopWg.Add(1)
go startHTTPServer(&stopWg, shutdownCh, errCh)

stopWg.Add(1)
go startRPCServer(&stopWg, shutdownCh, errCh)

// Listen for termination signal
quit := make(chan os.Signal)
signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

// Wait for error or OS quit signal
select {
case err := <-errCh:
    // TODO: Log error here
case <-quit:
    // wait for termination signal
}

// Notify all servers of shutdown by closing the shutdownCh
close(shutdownCh)

// Wait for servers shutdown to complete
stopWg.Wait()
```
