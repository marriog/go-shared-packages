package httpmet

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/hashicorp/go-sockaddr/template"
	"github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
)

const (
	keyMetricInfo = iota
)

var (
	httpReqTotalCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "perkbox_http_requests_total",
			Help: "Total HTTP requests counter.",
		},
		[]string{"service_name", "host", "path", "method", "status"},
	)

	httpReqErrCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "perkbox_http_requests_error_total",
			Help: "Total HTTP error requests counter.",
		},
		[]string{"service_name", "host", "path", "method", "status"},
	)

	httpReqDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "perkbox_http_request_duration_seconds",
			Help: "Duration of completed HTTP requests.",
			Buckets: []float64{
				00.100, 00.200, 00.300, 00.400, 1, 2, 3, 4, 5,
			},
		},
		[]string{"service_name", "host", "path", "method", "status"},
	)

	httpRespSize = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "perkbox_http_response_size_bytes",
			Help:    "Size of HTTP response body",
			Buckets: prometheus.ExponentialBuckets(32, 32, 6),
		},
		[]string{"service_name", "host", "path", "method", "status"},
	)

	httpMetricsOnce sync.Once
)

type HTTPMetricInfo struct {
	metricPath string
}

func HTTPMetricInfoFromContext(ctx context.Context) *HTTPMetricInfo {
	return ctx.Value(keyMetricInfo).(*HTTPMetricInfo)
}

// Metrics publishes Prometheus metrics for the HTTP interface
func Metrics() func(next http.Handler) http.Handler {
	httpMetricsOnce.Do(func() {
		prometheus.MustRegister(httpReqTotalCount)
		prometheus.MustRegister(httpReqErrCount)
		prometheus.MustRegister(httpReqDuration)
		prometheus.MustRegister(httpRespSize)
	})

	serviceName := os.Getenv("PERKBOX_SERVICE_NAME")
	host, err := template.Parse(`{{ GetPrivateIP }}`)
	if err != nil {
		log.Println("could not get host IP address. Default to 127.0.0.1")
		host = "127.0.0.1"
	}

	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			startTime := time.Now()

			metricInfo := &HTTPMetricInfo{
				metricPath: r.URL.Path,
			}
			rw := wrapResponseWriter(w)
			defer func() {
				method := r.Method
				status := strconv.Itoa(rw.Status())
				latency := time.Since(startTime)
				labels := []string{serviceName, host, metricInfo.metricPath, method, status}

				if rw.Status() >= 400 {
					httpReqErrCount.WithLabelValues(labels...).Inc()
				}
				httpReqTotalCount.WithLabelValues(labels...).Inc()
				httpReqDuration.WithLabelValues(labels...).Observe(latency.Seconds())
				httpRespSize.WithLabelValues(labels...).Observe(float64(rw.SizeBytes()))
			}()

			// Serve the request
			ctx := context.WithValue(r.Context(), keyMetricInfo, metricInfo)
			next.ServeHTTP(rw, r.WithContext(ctx))
		}
		return http.HandlerFunc(fn)
	}
}

func wrapResponseWriter(w http.ResponseWriter) *metricsResponseWriter {
	return &metricsResponseWriter{
		ResponseWriter: w,
	}
}

type metricsResponseWriter struct {
	http.ResponseWriter
	statusCode int
	sizeBytes  int
}

func (w *metricsResponseWriter) Status() int {
	return w.statusCode
}

func (w *metricsResponseWriter) SizeBytes() int {
	return w.sizeBytes
}

func (w *metricsResponseWriter) Header() http.Header {
	return w.ResponseWriter.Header()
}

func (w *metricsResponseWriter) Write(data []byte) (int, error) {
	n, err := w.ResponseWriter.Write(data)
	w.sizeBytes += n
	return n, err
}

func (w *metricsResponseWriter) WriteHeader(statusCode int) {
	w.statusCode = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}
