package httpmet

import (
	"net/http"
)

// MetricNameOverride sets the metrics path for a specified route. This is necessary for routes with dynamic components in them
// as we want to avoid creating unnecessary shards in the Prometheus database.
// For instance a route like /users/{userID} should NOT send metrics as /users/1234 or /users/4567
func MetricNameOverride(name string, next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		metricInfo := HTTPMetricInfoFromContext(r.Context())
		metricInfo.metricPath = name
		next.ServeHTTP(w, r)
	})
}
