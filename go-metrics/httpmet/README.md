# httpmet

## Installation

```go
import "git.perkbox.io/go-shared-packages/go-metrics/httpmet"
```

## How to instrument HTTP server

Add middleware to handle Prometheus requests

```go
router.Use(httpmet.Metrics())
```

Add connection tracking to `http.Server`

> **NOTE:** Connection counter internally starts a goroutine to publish metrics
periodically that needs to be cleanly shutdown via the shutdown channel.

```go
server := &http.Server{...}
httpmet.TrackConnMetrics(server, shutdownCh)
```

Route paths with dynamic parts must be normalized

```go
router.Post("/v1/{tenant}/resources", httpmet.MetricNameOverride("/v1/{tenant}/resources", handler))
```
