package httpmet

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/hashicorp/go-sockaddr/template"
	"github.com/prometheus/client_golang/prometheus"
)

const (
	httpConnStateSnapshotInterval = 5 * time.Second
)

var (
	httpConnStateCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "perkbox_http_connections",
			Help: "Gauge HTTP connection state",
		},
		[]string{"service_name", "host", "state"},
	)

	httpConnMetricsOnce sync.Once
)

func TrackConnMetrics(server *http.Server, shutdownCh <-chan struct{}) {
	httpConnMetricsOnce.Do(func() {
		prometheus.MustRegister(httpConnStateCount)
	})

	host, err := template.Parse(`{{ GetPrivateIP }}`)
	if err != nil {
		log.Print("could not get host IP address. Default to 127.0.0.1")
		host = "127.0.0.1"
	}

	t := httpConnTracker{
		serviceName:       os.Getenv("PERKBOX_SERVICE_NAME"),
		host:              host,
		delegateConnState: server.ConnState,
		newCh:             make(chan net.Conn),
		activeCh:          make(chan net.Conn),
		idleCh:            make(chan net.Conn),
		closedCh:          make(chan net.Conn),
	}
	server.ConnState = t.trackConnState
	go t.track(shutdownCh)
}

type httpConnTracker struct {
	serviceName       string
	host              string
	delegateConnState func(net.Conn, http.ConnState)
	newCh             chan net.Conn
	activeCh          chan net.Conn
	idleCh            chan net.Conn
	closedCh          chan net.Conn
	newCount          int64
	activeCount       int64
	idleCount         int64
	shutdownLock      sync.RWMutex
	shutdown          bool
}

func (t *httpConnTracker) trackConnState(c net.Conn, connState http.ConnState) {
	if t.delegateConnState != nil {
		t.delegateConnState(c, connState)
	}

	// prevent writing to internal channels if we are shutting down
	if t.isTrackerShutdown() {
		return
	}

	switch connState {
	case http.StateNew:
		t.newCh <- c
	case http.StateActive:
		t.activeCh <- c
	case http.StateIdle:
		t.idleCh <- c
	case http.StateHijacked, http.StateClosed:
		t.closedCh <- c
	}
}

func (t *httpConnTracker) track(shutdownCh <-chan struct{}) {
	defer func() {
		close(t.newCh)
		close(t.activeCh)
		close(t.idleCh)
		close(t.closedCh)
	}()

	connsStateMap := map[net.Conn]http.ConnState{}

	decConn := func(c net.Conn) {
		switch connsStateMap[c] {
		case http.StateNew:
			atomic.AddInt64(&t.newCount, -1)
		case http.StateActive:
			atomic.AddInt64(&t.activeCount, -1)
		case http.StateIdle:
			atomic.AddInt64(&t.idleCount, -1)
		default:
			panic(fmt.Errorf("unknown existing connection: %s", c))
		}
	}

	ticker := time.NewTicker(httpConnStateSnapshotInterval)

	var newSnapCount, activeSnapCount, idleSnapCount, totalSnapCount int64
	for {
		select {
		case <-shutdownCh:
			t.trackerShutdown()
			ticker.Stop()
			return
		case <-ticker.C:
			newSnapCount = atomic.LoadInt64(&t.newCount)
			httpConnStateCount.
				WithLabelValues(t.serviceName, t.host, "new").
				Set(float64(newSnapCount))

			activeSnapCount = atomic.LoadInt64(&t.activeCount)
			httpConnStateCount.
				WithLabelValues(t.serviceName, t.host, "active").
				Set(float64(activeSnapCount))

			idleSnapCount = atomic.LoadInt64(&t.idleCount)
			httpConnStateCount.
				WithLabelValues(t.serviceName, t.host, "idle").
				Set(float64(idleSnapCount))

			totalSnapCount = newSnapCount + activeSnapCount + idleSnapCount
			httpConnStateCount.
				WithLabelValues(t.serviceName, t.host, "total").
				Set(float64(totalSnapCount))
		case c := <-t.newCh:
			connsStateMap[c] = http.StateNew
			atomic.AddInt64(&t.newCount, 1)
		case c := <-t.activeCh:
			decConn(c)
			atomic.AddInt64(&t.activeCount, 1)
			connsStateMap[c] = http.StateActive
		case c := <-t.idleCh:
			decConn(c)
			atomic.AddInt64(&t.idleCount, 1)
			connsStateMap[c] = http.StateIdle
		case c := <-t.closedCh:
			decConn(c)
			delete(connsStateMap, c)
		}
	}
}

func (t *httpConnTracker) trackerShutdown() {
	t.shutdownLock.Lock()
	defer t.shutdownLock.Unlock()

	if t.shutdown {
		return
	}
	t.shutdown = true
}

func (t *httpConnTracker) isTrackerShutdown() bool {
	t.shutdownLock.RLock()
	defer t.shutdownLock.RUnlock()

	return t.shutdown
}
