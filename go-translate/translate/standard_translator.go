package translate

import "net/http"

// NewStandardTranslator returns a translator.
// Expects a frontendDomain in the format of "perkbox.com", "perkbox.net" etc.
func NewStandardTranslator(frontendDomain string, httpClient *http.Client) Translator {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	return &standardTranslator{
		httpClient:     httpClient,
		frontendDomain: frontendDomain,
	}
}

type standardTranslator struct {
	httpClient     *http.Client
	frontendDomain string
}

// LoadMappings will return the set of mappings for the given environment, locale and tag
func (t *standardTranslator) LoadMappings(locale string, tag string) (*Mappings, error) {
	return loadFile(translationFileURL(t.frontendDomain, locale, tag), t.httpClient)
}
