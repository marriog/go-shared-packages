package translate

import (
	"encoding/json"
	"fmt"
	"sync"
)

type Mappings struct {
	mu           sync.Mutex
	mappings     []Mapping
	mappingsByID map[string]*Mapping
}

// GetByID returns the mapping with the given ID. If none found, Valid is false
func (m *Mappings) GetByID(id string) Mapping {
	m.mu.Lock()
	defer m.mu.Unlock()

	if m.mappingsByID == nil {
		m.mappingsByID = make(map[string]*Mapping)
	}
	if ma, ok := m.mappingsByID[id]; ok {
		return *ma
	}
	return Mapping{ID: id, Valid: false}
}

// UnmarshalJSON unmarshal's a byte array into the expected data set
func (m *Mappings) UnmarshalJSON(data []byte) error {
	var mappingsMap map[string]string

	err := json.Unmarshal(data, &mappingsMap)
	if err != nil {
		return fmt.Errorf("could not unmarshal mappings: %s", err)
	}

	for id, val := range mappingsMap {
		m.Add(id, val)
	}

	return nil
}

// Add adds a new mapping to the collection
func (m *Mappings) Add(id string, value string) *Mappings {
	m.mu.Lock()
	defer m.mu.Unlock()

	if m.mappings == nil {
		m.mappings = make([]Mapping, 0)
	}
	if m.mappingsByID == nil {
		m.mappingsByID = make(map[string]*Mapping)
	}
	m.mappings = append(m.mappings, Mapping{
		ID:    id,
		Value: value,
		Valid: value != "",
	})
	m.mappingsByID[id] = &m.mappings[len(m.mappings)-1]

	return m
}
