package translate

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// Translator is used to load mappings for a given environment
type Translator interface {
	// LoadMappings will return the set of mappings for the given environment, locale and tag
	LoadMappings(locale string, tag string) (*Mappings, error)
}

// translationFileURL returns the URL address for the related translation file for the given arguments
func translationFileURL(frontendDomain string, locale string, tag string) string {
	return fmt.Sprintf("%s/locale/web/%s/%s.json", frontendDomain, strings.ToLower(locale), strings.ToLower(tag))
}

// loadFile will read, parse and return the mappings at the given path.
func loadFile(path string, httpClient *http.Client) (*Mappings, error) {
	r, err := httpClient.Get(path)
	if err != nil {
		return nil, fmt.Errorf("could not get translation file (%s): %s", path, err)
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	bodyStr := "unparsed body"
	if err == nil {
		bodyStr = string(body)
	}

	if r.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("invalid status code response when getting translation file: %d: %s", r.StatusCode, bodyStr)
	}

	if err != nil {
		return nil, fmt.Errorf("could not read body of translation file (%s): %s", path, err)
	}

	var mappings Mappings
	err = json.Unmarshal(body, &mappings)
	if err != nil {
		return nil, fmt.Errorf("could not json decode mappings: %s", err)
	}

	return &mappings, nil
}
