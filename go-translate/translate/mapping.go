package translate

// Mapping represents a translation from ID to Value
type Mapping struct {
	ID    string
	Value string
	Valid bool
}

// ValueOrDefault returns the mappings value, or def if it is blank
func (m Mapping) ValueOrDefault(def string) string {
	if !m.Valid {
		return def
	}
	return m.Value
}

// ValueOrID returns the mappings value, or the content id if it is blank
func (m Mapping) ValueOrID() string {
	if !m.Valid {
		return m.ID
	}
	return m.Value
}
