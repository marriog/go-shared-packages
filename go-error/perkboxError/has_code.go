package perkboxError

// HasCode returns true if the given error has one of the error codes provided in codes
func HasCode(err Error, codes []string) bool {
	for _, code := range codes {
		if err.Code() == code {
			return true
		}
	}
	return false
}
