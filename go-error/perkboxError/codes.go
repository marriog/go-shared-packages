package perkboxError

const (
	// Generic
	UnknownError        = "PBERRUnknownError"
	InternalServerError = "PBERRInternalServerError"

	// Auth
	TokenMissing                    = "PBERRTokenMissing"
	TokenBuilderError               = "PBERRTokenBuilderError"
	ForbiddenNoPermission           = "PBERRForbiddenNoPermission"
	ForbiddenRestrictionCheckFailed = "PBERRForbiddenRestrictionCheckFailed"
	ForbiddenInvalidToken           = "PBERRForbiddenInvalidToken"
	ForbiddenTokenExpired           = "PBERRForbiddenTokenExpired"
	ForbiddenUnexpectedRestriction  = "PBERRForbiddenUnexpectedRestriction"
)
