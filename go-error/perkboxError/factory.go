package perkboxError

func New(code string) *BaseError {
	return &BaseError{
		ErrCode:          code,
		ErrData:          make(map[string]interface{}),
		ErrStatus:        0,
		ErrLegacyMessage: "",
		ErrInternal:      "",
		ErrCorrelation:   "",
	}
}

func FromErr(err error) *BaseError {
	if err == nil {
		return nil
	}
	if pbErr, ok := err.(*BaseError); ok {
		return pbErr
	}
	return New(UnknownError).
		WithInternal(err.Error())
}
