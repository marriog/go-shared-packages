package perkboxError

import (
	"fmt"
)

type Error interface {
	Error() string
	Code() string
	Status() int
	LegacyMessage() string
	InternalMessage() string
	Data() map[string]interface{}
	CorrelationId() string
}

type BaseError struct {
	ErrCode          string                 `json:"code"`
	ErrStatus        int                    `json:"status"`
	ErrLegacyMessage string                 `json:"message"` // will be deprecated
	ErrInternal      string                 `json:"dev_message"`
	ErrData          map[string]interface{} `json:"data"`
	ErrCorrelation   string                 `json:"reference"`
}

func example(devMsg string) *BaseError {
	return &BaseError{
		ErrCode:     TokenMissing,
		ErrStatus:   401,
		ErrInternal: devMsg,
	}
}

func (x BaseError) Code() string {
	return x.ErrCode
}

func (x BaseError) Status() int {
	return x.ErrStatus
}

func (x BaseError) LegacyMessage() string {
	return x.ErrLegacyMessage
}

func (x BaseError) Data() map[string]interface{} {
	return x.ErrData
}

func (x BaseError) InternalMessage() string {
	return x.ErrInternal
}

func (x BaseError) CorrelationId() string {
	return x.ErrCorrelation
}

func (x *BaseError) WithCode(c string) *BaseError {
	x.ErrCode = c
	return x
}

func (x *BaseError) WithStatus(s int) *BaseError {
	x.ErrStatus = s
	return x
}

func (x *BaseError) WithInternal(msg string) *BaseError {
	if msg != "" {
		x.ErrInternal += msg
	}

	return x
}

func (x *BaseError) WithCorrelationId(id string) *BaseError {
	x.ErrCorrelation = id
	return x
}

func (x *BaseError) WithLegacyMessage(msg string) *BaseError {
	x.ErrLegacyMessage = msg
	return x
}

func (x *BaseError) WithDataItem(key string, value interface{}) *BaseError {
	if x.ErrData == nil {
		x.ErrData = make(map[string]interface{})
	}

	x.ErrData[key] = value
	return x
}

func (x BaseError) Error() string {
	return fmt.Sprintf("[BaseError] code: %s | status: %d | dev: %s | reference: %s | legacyMessage: %s | data: %s",
		x.ErrCode, x.ErrStatus, x.ErrInternal, x.ErrCorrelation, x.ErrLegacyMessage, fmt.Sprint(x.ErrData))
}
